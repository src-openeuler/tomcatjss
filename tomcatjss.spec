Name:             tomcatjss
Summary:          Apache Tomcat of JSS Connector
URL:              http://www.dogtagpki.org/wiki/TomcatJSS
License:          LGPLv2+
BuildArch:        noarch
Version:          8.4.1
Release:          2
Source:           https://github.com/dogtagpki/tomcatjss/archive/v%{version}/%{name}-%{version}.tar.gz
BuildRequires:    ant apache-commons-lang3 java-latest-openjdk-devel slf4j
BuildRequires:    slf4j-jdk14 jss >= 4.6.0 tomcat >= 1:9.0.7
Requires:         apache-commons-lang3 java-latest-openjdk-headless jpackage-utils >= 0:1.7.5-15 slf4j slf4j-jdk14 jss >= 5.2.0 tomcat >= 1:9.0.7
Conflicts:        pki-base < 10.10.0
%define           _sharedstatedir    /var/lib

Patch1:           0002-keep-deprecated-constructors.patch

%description
Apache Tomcat of JSS Connector.

%prep
%autosetup -n tomcatjss-%{version} -p 1

%build
openjdk_latest_version=`rpm -qi java-latest-openjdk-headless | grep Version | cut -b 15-16`
home_path=`ls /usr/lib/jvm | grep java-${openjdk_latest_version}-openjdk-${openjdk_latest_version}`
export JAVA_HOME=/usr/lib/jvm/${home_path}
./build.sh \
    %{?_verbose:-v} \
    --work-dir=%{_vpath_builddir} \
    --version=%{version} \
    --jni-dir=%{_jnidir} \
    dist

%install
openjdk_latest_version=`rpm -qi java-latest-openjdk-headless | grep Version | cut -b 15-16`
home_path=`ls /usr/lib/jvm | grep java-${openjdk_latest_version}-openjdk-${openjdk_latest_version}`
export JAVA_HOME=/usr/lib/jvm/${home_path}
./build.sh \
    %{?_verbose:-v} \
    --work-dir=%{_vpath_builddir} \
    --version=%{version} \
    --java-dir=%{_javadir} \
    --doc-dir=%{_docdir} \
    --install-dir=%{buildroot} \
    install

%files
%defattr(-,root,root)
%doc README LICENSE
%{_javadir}/*

%changelog
* Wed Nov 13 2024 wangkai <13474090681@163.com> - 8.4.1-2
- Fix build fail for tomcat upgrade

* Tue Oct 08 2024 Ge Wang <wang__ge@126.com> - 8.4.1-1
- Upgrade to 8.4.1

* Fri Mar 31 2023 lilong <lilong@kylinos.cn> - 8.2.0-1
- Upgrade to 8.2.0

* Mon Nov 21 2022 wulei <wulei80@h-partners.com> - 8.1.0-2
- Rectify the tomcatjss compilation failure caused by the openjdk-latest upgrade

* Wed Jun 08 2022 Ge Wang <wangge20@h-partners.com> - 8.1.0-1
- Upgrade to version 8.1.0

* Tue Dec 07 2021 wangkai <wangkai385@huawei.com> - 7.4.1-5
- Remove conflicts tomcat-native

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 7.4.1-4
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Sun Apr 26 2020 wutao <wutao61@huawei.com> - 7.4.1-3
- Package init
